networking.firewall.enable = false;

# Mount the NFS
fileSystems."/data" = {
  device = "server:/";
  fsType = "nfs";
};
