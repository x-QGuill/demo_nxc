{ pkgs, setup,  ... }: {

  nodes = {
    node = { pkgs, ... }:
	{
      # add needed package
      environment.systemPackages = with pkgs; [ ior openmpi ];
    };
  };
  testScript = ''
  '';
}
