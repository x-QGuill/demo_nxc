networking.firewall.enable = false;

# Enable the nfs server services
services.nfs.server.enable = true;

# Define a mount point at /srv/shared
services.nfs.server.exports = ''
  /my_nfs/shared *(rw,no_subtree_check,fsid=0,no_root_squash)
'';
services.nfs.server.createMountPoints = true;
