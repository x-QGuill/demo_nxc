{ pkgs, setup,  ... }: {

  nodes = {
    node = { pkgs, ... }:
	{
      # Packages
      environment.systemPackages = with pkgs; [ ior openmpi ];

      # Open ports
      networking.firewall.enable = false;

      # Mount the NFS
      fileSystems."/data" = {
        device = "server:/";
        fsType = "nfs";
      };
    };

    server = { pkgs, ... }:
    {
      networking.firewall.enable = false;

      # Enable the nfs server services
      services.nfs.server.enable = true;

      # Define a mount point at /srv/shared
      services.nfs.server.exports = ''
        /srv/shared *(rw,no_subtree_check,fsid=0,no_root_squash)
      '';
      services.nfs.server.createMountPoints = true;
    };
  };
  testScript = ''
  '';
}
